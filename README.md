# Script de configuration post-installation pour les serveurs CentOS 7

(c) Franck Lafon, 2020

Ce repository fournit un script "automagique" de configuration post-installation pour les serveurs exécutant CentOS 7 ainsi qu'une collection de scripts d'aide et de modèles de fichiers de configuration pour les services communs.

## En bref

Perform the following steps.

  1. Installez un système CentOS 7 minimal.

  2. Créer un utilisateur non "root" avec des privilèges d'administrateur.

  3. Installer le paquet Git: `sudo yum install git`

  4. récupérer le script sur Git: `git clone https://gitlab.com/kikinovak/centos-7.git`

  5. Aller dans le dossier: `cd centos-7`

  6. démarrer le script: `sudo ./centos-setup.sh --setup`

  7. Prenez une tasse de café pendant que le script fait tout le travail.

  8. redemerer le poste.

## Personnalisation d'un serveur CentOS

La transformation d'une installation CentOS minimale en un serveur fonctionnel se résume toujours à une série d'opérations plus ou moins longues. Votre parcours peut bien sûr varier, mais voici ce que je fais habituellement sur une nouvelle installation CentOS:

* Personnalisez le shell Bash : prompt, alias, etc.

* Personnalisez l'éditeur Vim.

* Mettre en place des dépôts officiels et des dépôts de tiers.

* Installez un ensemble complet d'outils en ligne de commande.

* Retirer les paquets inutiles.

* Permettre à l'utilisateur administrateur d'accéder aux journaux du système.

* Désactivez IPv6 et reconfigurez certains services en conséquence.
  
* Configurer un mot de passe persistant pour `sudo`.

* Etc.

Le script `centos-setup.sh` effectue toutes ces opérations.

Configurez Bash et Vim et définissez une résolution de console par défaut plus lisible :

```bash
# ./centos-setup.sh --shell
```

Setup official and third-party repositories:

```bash
# ./centos-setup.sh --repos
```

Install the `Core` and `Base` package groups along with some extra tools:

```bash
# ./centos-setup.sh --extra
```

Remove a handful of unneeded packages:

```bash
# ./centos-setup.sh --prune
```

Enable the admin user to access system logs:

```bash
# ./centos-setup.sh --logs
```

Disable IPv6 and reconfigure basic services accordingly:

```bash
# ./centos-setup.sh --ipv4
```

Configure password persistence for sudo:

```bash
# ./centos-setup.sh --sudo
```

Perform all of the above in one go:

```bash
# ./centos-setup.sh --setup
```

Strip packages and revert back to an enhanced base system:

```bash
# ./centos-setup.sh --strip
```

Display help message:

```bash
# ./centos-setup.sh --help
```

If you want to know what exactly goes on under the hood, open a second terminal
and view the logs:

```bash
tail -f /tmp/centos-setup.log
```
